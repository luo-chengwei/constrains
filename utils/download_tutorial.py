#!/usr/bin/env/python
# -*- coding: utf-8 -*- 

__author__ = 'Chengwei Luo (cluo@broadinstitute.org)'
__version__ = '0.0.6'
__date__ = 'Feb 2015'

"""
ConStrains: identifying Conspecific Strains with metagenomic reads

Copyright(c) 2013 Chengwei Luo (cluo@broadinstitute.org)

	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>

https://bitbucket.org/luo-chengwei/constrains

for help, type:
python ConStrains.fit.py --help
"""

USAGE = \
"""Usage: %prog -o <output_dir>

ConStrains: identifying Conspecific Strains with metagenomic reads

This is a utility script to download the tutorial data to designated directory.

Add --help to see a full list of required and optional
arguments to run ConStrains.

Additional information can also be found at:
https://bitbucket.org/luo-chengwei/constrains/wiki

If you use ConStrains in your work, please cite it as:
<ConStrains citation here>

Copyright: Chengwei Luo, Broad Institute of MIT and Harvard, 2014.
"""

import sys, os, re, ftplib
from optparse import OptionParser, OptionGroup

def main():
	parser = OptionParser(usage = USAGE, version="Version: " + __version__)

	compOptions = OptionGroup(parser, "Compulsory parameters",
						"There options are compulsory, and may be supplied in any order.")

	compOptions.add_option("-o", "--outdir", type = "string", metavar = 'DIR',
							help = "Output directory for where the files will be downloaded to")

	parser.add_option_group(compOptions)
	
	options, strings = parser.parse_args(sys.argv)
	
	outdir = options.outdir
	if not os.path.exists(outdir): os.mkdir(outdir)
	
	path = '/'
	filenames = ['fq.tar.gz','test.conf']
	ftp = ftplib.FTP("ftp.broadinstitute.org") 
	ftp.login("ConStrains", "ConStrains") 
	ftp.cwd(path)
	for filename in filenames:
		outfile = outdir + '/' + filename
		ftp.retrbinary("RETR " + filename ,open(outfile, 'wb').write)
	ftp.quit()

if __name__ == '__main__': main()


